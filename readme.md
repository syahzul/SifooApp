# SifooApp - Cordova

Projek lengkap untuk kursus Pembangunan Aplikasi Mobile: Cordova Tahap 1 di [Sifoo Training Solutions](http://sifoo.com).

## Notis Hakcipta

Logo dan imej yang terdapat di dalam projek ini adalah hak milik [Sifoo Training Solutions](http://sifoo.com).

Kod-kod aturcara yang terdapat di dalam projek ini merupakan hak cipta [Syahril Zulkefli](http://www.syahzul.com).

Sebarang penggunaan diluar kursus haruslah mendapat persetujuaan daripada kedua-dua pemegang hak cipta terlebih dahulu.